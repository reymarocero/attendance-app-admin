import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Login from './src/screens/Login';

import { NativeWindStyleSheet } from "nativewind";
import Dashboard from './src/screens/Dashboard';
import HeaderComponent from './src/components/HeaderComponent';
import Students from './src/screens/Students';
import AddStudent from './src/screens/AddStudent';
import Teachers from './src/screens/Teachers';
import AddTeacher from './src/components/AddTeacher';
import Scheduler from './src/screens/Scheduler';
import AddSchedule from './src/components/AddSchedule';
import ViewSchedule from './src/screens/ViewSchedule';
import SubjectsPerLevel from './src/screens/SubjectsYearLevel';
import ViewStudent from "./src/screens/ViewStudent";
import Settings from './src/screens/Settings';

NativeWindStyleSheet.setOutput({
  default: "native",
});

const Stack = createNativeStackNavigator();


export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        ></Stack.Screen>
        <Stack.Screen
          name="Dashboard"
          component={Dashboard}
          options={{
            header: () => (
              <HeaderComponent title="Dashboard" disableBack={true} />
            ),
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="Students"
          component={Students}
          options={{
            header: () => <HeaderComponent title="Students" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="AddStudent"
          component={AddStudent}
          options={{
            header: () => <HeaderComponent title="Add Student" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="Teachers"
          component={Teachers}
          options={{
            header: () => <HeaderComponent title="Teachers" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="AddTeacher"
          component={AddTeacher}
          options={{
            header: () => <HeaderComponent title="Add Teacher" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="Scheduler"
          component={Scheduler}
          options={{
            header: () => <HeaderComponent title="Scheduler" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="AddSchedule"
          component={AddSchedule}
          options={{
            header: () => <HeaderComponent title="Add Schedule" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="ViewSchedule"
          component={ViewSchedule}
          options={{
            header: () => <HeaderComponent title="View Schedule" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="SubjectsPerLevel"
          component={SubjectsPerLevel}
          options={{
            header: () => <HeaderComponent title="Add Subjects per Level" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="ViewStudent"
          component={ViewStudent}
          options={{
            header: () => <HeaderComponent title="View Student" />,
          }}
        ></Stack.Screen>
        <Stack.Screen
          name="Settings"
          component={Settings}
          options={{
            header: () => <HeaderComponent title="Settings" />,
          }}
        ></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}