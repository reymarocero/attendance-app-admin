import { useEffect, useState } from 'react';
import { FlatList, Text, View, ActivityIndicator, TouchableOpacity } from 'react-native';
import { supabase } from '../lib/supabase';
import { useNavigation } from '@react-navigation/native';

import Ionicons from "@expo/vector-icons/Ionicons";

const Teachers = () => {
    const nav = useNavigation();

    const [teachers, setTeachers] = useState([]);
    const [loading, setLoading] = useState(false);

    const getTeachersData = async () => {

        setLoading(true);

        const { data, error } = await supabase.from("teacher").select("*");

        if (error) {
            console.error("Error on retrieving table", error);
            return;
        }

        setTeachers(data);

        setLoading(false);
    }

    const handleRenderTeacher = ({item}) => {
        return (
          <TouchableOpacity key={item.id}>
            <View className="py-3 border-gray-100 border-b px-4">
              <Text className="font-bold text-gray-700">{item.full_name}</Text>
            </View>
          </TouchableOpacity>
        );
    }

    useEffect(() => {
        const init = async () => {
            await getTeachersData();
        }

        init();
    },[]);

    return (
      <View className="mx-5 mt-5">
        {loading ? (
          <ActivityIndicator style={{ marginTop: 40 }} />
        ) : (
          <View className="rounded-2xl bg-white mt-5">
            <View className="flex flex-row items-center justify-between p-4 bg-[#F1F9FF] rounded-2xl rounded-b-none">
              <View className="flex flex-row gap-2 items-center">
                <Text>{teachers.length} Teachers</Text>
                <Ionicons
                  name="refresh"
                  size={20}
                  color="blue"
                  onPress={getTeachersData}
                />
              </View>
              <TouchableOpacity
                className="h-[40px] w-auto px-3 flex items-center justify-center rounded-full bg-[#1E8CE1]"
                onPress={() => nav.navigate("AddTeacher")}
              >
                <Text className="text-white">Add Teacher</Text>
              </TouchableOpacity>
            </View>

            <FlatList data={teachers} renderItem={handleRenderTeacher} />
          </View>
        )}
      </View>
    );
}

export default Teachers;