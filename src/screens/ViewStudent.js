import { useEffect, useState } from "react";
import { View, TouchableOpacity, Text, TextInput } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import { supabase } from "../lib/supabase";
import { qrPdfHTML } from "../lib/generateQRtoPDF";

import * as FileSystem from "expo-file-system";
import * as Print from "expo-print";
import * as Sharing from "expo-sharing";
import * as MediaLibrary from "expo-media-library";

import { Alert } from "../components/Alert";

const ViewStudent = ({ navigation, route }) => {

    const { id } = route.params;

    const [student, setStudent] = useState([]);
    const [studentId, setStudentId] = useState("");
    const [fullname, setFullname] = useState("");
    const [parentName, setParentName] = useState("");
    const [parentNumber, setParentNumber] = useState("");
    const [currentStatus, setCurrentStatus] = useState("");
    const [successSave, setSuccessSave] = useState(false);
    const [successDownload, setSuccessDownload] = useState(false);
    const [successDeactivate, setSuccessDeactivate] = useState(false);

    const [year, setYear] = useState([]);
    const [yearOpen, setYearOpen] = useState(false);
    const [yearVal, setYearVal] = useState("");
    const [section, setSection] = useState([
      { label: "Section A", value: "Section A" },
      { label: "Section B", value: "Section B" },
      { label: "Section C", value: "Section C" },
    ]);
    const [sectionOpen, setSectionOpen] = useState(false);
    const [sectionVal, setSectionVal] = useState("");

    const yearArray = [];
    const _thtml = qrPdfHTML(student);

    const handleGetYear = async () => {
      const { data, error } = await supabase.from("year_level").select("*");

      if (error) {
        console.error(error);
        return;
      }

      data.map((item) => {
        yearArray.push({
          label: item.level_name,
          value: item.level_code,
        });
      });

      setYear(yearArray);
    };

    const getStudentsData = async () => {
        const { data, error } = await supabase
            .from("enrolles")
            .select("*")
            .eq("id", id);

        if (error) {
            console.error(error);
            return;
        }

        setStudent(data);
        setStudentId(data[0].student_id);
        setFullname(data[0].fullname);
        setParentName(data[0].guardian_name);
        setParentNumber(data[0].guardian_contact);
        setYearVal(data[0].year_level);
        setSectionVal(data[0].section_name);
        setCurrentStatus(data[0].current_status);
    }

    const handleUpdate = async () => {
        const { error } = await supabase
            .from("enrolles")
            .update({
                fullname: fullname,
                guardian_contact: parentNumber,
                student_id: studentId,
                guardian_name: parentName,
                year_level: yearVal,
                section_name: sectionVal
            })
            .eq("id", id);
        
        if (error) {
            console.error(error);
        }

        console.log("Success update");
        setSuccessSave(true);
        setSuccessDownload(false);
    }

    const handleDownLoadQR = async () => {
      await createAndSavePDF(_thtml);
    };

    const createAndSavePDF = async (html) => {
      try {
        const dateNow = new Date();
        const { uri } = await Print.printToFileAsync({ html });
        const newUri = `${uri.slice(
          0,
          uri.indexOf("/Print/") + 1
        )}Print/qrcodes_${yearVal}_${dateNow.getTime()}.pdf`;

        if (Platform.OS === "ios") {
          await Sharing.shareAsync(uri);
        } else {
            const permission = await MediaLibrary.requestPermissionsAsync();

          if (permission.granted) {
            await FileSystem.moveAsync({
              from: uri,
              to: newUri,
            });

            const asset = await MediaLibrary.createAssetAsync(newUri);
            await MediaLibrary.createAlbumAsync("AttendanceApp", asset, false);

            setSuccessDownload(true);
            setSuccessSave(false);
          }
        }
      } catch (error) {
        console.error(error);
      }
    };
  
    const handleDeactivate = async (isDeactivate) => {
      const { error } = await supabase
        .from("enrolles")
        .update({
          current_status: isDeactivate ? "DEACTIVATED" : "ACTIVATE"
        })
        .eq("id", id);
      
      if (error) {
        console.error(error);
        return;
      }

      if (isDeactivate) {
        setCurrentStatus("DEACTIVATED");
      } else {
        setCurrentStatus("");
      }

      setSuccessDeactivate(true);
      setSuccessDownload(false);
      setSuccessSave(false);
    }

    useEffect(() => {
        const init = async () => {
            await getStudentsData();
            await handleGetYear();
        }

        init();
    },[])
    return (
      <View>
        <View className="mx-5 mt-5">
          {successSave && (
            <Alert
              title="Success"
              message="Successfully update student."
              type="success"
            />
          )}
          {successDownload && (
            <Alert
              title="Success"
              message="Downdload complete, you can check your file at /Picture/AttendanceApp/"
              type="success"
            />
          )}
          {successDeactivate && (
            <Alert
              title="Success"
              message="Successfully deactivated the student."
              type="success"
            />
          )}
          <View className="grid gap-4">
            <View className="flex flex-row justify-between">
              {currentStatus === "ACTIVE" || currentStatus === "" ? (
                <TouchableOpacity
                  onPress={() => handleDeactivate(true)}
                  className="h-[30px] w-auto px-3 flex items-center justify-center rounded-full bg-red-400"
                >
                  <Text className="text-white text-[13px]">Deactivate</Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => handleDeactivate(false)}
                  className="h-[30px] w-auto px-3 flex items-center justify-center rounded-full bg-green-700"
                >
                  <Text className="text-white text-[13px]">Activate</Text>
                </TouchableOpacity>
              )}
              <TouchableOpacity
                onPress={handleDownLoadQR}
                className="h-[30px] w-auto px-3 flex items-center justify-center rounded-full bg-[#1E8CE1]"
              >
                <Text className="text-white text-[13px]">Download QR</Text>
              </TouchableOpacity>
            </View>
            <TextInput
              onChange={(e) => setStudentId(e.nativeEvent.text)}
              placeholder="Student ID"
              className="p-4 border border-slate-600 rounded-md text-black bg-white"
              value={studentId}
            ></TextInput>
            <TextInput
              onChange={(e) => setFullname(e.nativeEvent.text)}
              placeholder="Full Name"
              className="p-4 border border-slate-600 rounded-md text-black bg-white"
              value={fullname}
            ></TextInput>
            <TextInput
              onChange={(e) => setParentName(e.nativeEvent.text)}
              placeholder="Parent Name"
              className="p-4 border border-slate-600 rounded-md text-black bg-white"
              value={parentName}
            ></TextInput>
            <TextInput
              onChange={(e) => setParentNumber(e.nativeEvent.text)}
              placeholder="Parent Number"
              className="p-4 border border-slate-600 rounded-md text-black bg-white"
              value={parentNumber}
            ></TextInput>
            <View className="relative z-40">
              <DropDownPicker
                open={yearOpen}
                setOpen={setYearOpen}
                items={year}
                setItems={setYear}
                placeholder="Select year level"
                setValue={setYearVal}
                value={yearVal}
              />
            </View>
            <View className="relative z-30">
              <DropDownPicker
                open={sectionOpen}
                setOpen={setSectionOpen}
                items={section}
                setItems={setSection}
                placeholder="Select section"
                setValue={setSectionVal}
                value={sectionVal}
              />
            </View>
            <View>
              <TouchableOpacity
                className="p-5 bg-[#3290D3] rounded-lg"
                onPress={handleUpdate}
              >
                <Text className="text-center text-white font-bold text-base">
                  Update Student Information
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
}

export default ViewStudent;