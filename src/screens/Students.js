import { View, Text, TouchableOpacity, FlatList, ActivityIndicator, StyleSheet, ScrollView } from 'react-native';
import Ionicons from "@expo/vector-icons/Ionicons";
import { useNavigation } from '@react-navigation/native';
import * as DocumentPicker from "expo-document-picker";
import * as FileSystem from "expo-file-system";
import * as Papa from "papaparse";
import * as Print from "expo-print";
import * as Sharing from "expo-sharing";
import * as MediaLibrary from "expo-media-library";

import { useEffect, useState } from 'react';
import { supabase } from '../lib/supabase';
import { qrPdfHTML } from '../lib/generateQRtoPDF';
import { Alert } from '../components/Alert';

const Students = () => {
  const nav = useNavigation();
  const [students, setStudents] = useState([]);
  const [loading, setLoading] = useState(false);
  const [yearLevels, setYearLevels] = useState([]);
  const [active, setActive] = useState(active);
  const [selectedLevel, setSelectedLevel] = useState("");
  const [success, setSuccess] = useState(false);
  const [successUpload, setSuccessUpload] = useState(false);

  const _thtml = qrPdfHTML(students);

  const handleBulkUpload = async () => {
    try {
      const res = await DocumentPicker.getDocumentAsync({});

      if (res.type === "cancel") {
        return;
      }

      const csvString = await FileSystem.readAsStringAsync(res.uri);

      const csvParse = Papa.parse(csvString, { header: true });

      let temp = []; // store temporary

      csvParse.data.map((item) => {
        temp.push({
          fullname: item["Full Name"],
          guardian_name: item["Guardian Name"],
          guardian_contact: item["Guardian Contact"],
          year_level: item["Year Level"],
          qrcode: "QR:" + item["Student ID"] + item["Full Name"],
          current_status: "ACTIVE",
          student_id: item["Student ID"],
          section_name: item["Section"],
        });
      });

      // save to database
      const { error } = await supabase
        .from("enrolles")
        .insert(temp);

      if (error) {
        console.error("Error on bulk upload", error);
        return;
      }

      console.log("Success on uploading enrolles.");
      setSuccessUpload(true);

    } catch (error) {
      console.error(error);
    }
  }

  const handleBulkDownloadQRs = async () => {
    await createAndSavePDF(_thtml);
  }

  const createAndSavePDF = async (html) => {
    try {
      const dateNow = new Date();
      const { uri } = await Print.printToFileAsync({ html });
      const newUri = `${uri.slice(0, uri.indexOf('/Print/') + 1)}Print/qrcodes_${selectedLevel}_${dateNow.getTime()}.pdf`;

      if (Platform.OS === "ios") {
        await Sharing.shareAsync(uri);
      } else {
        const permission = await MediaLibrary.requestPermissionsAsync();

        if (permission.granted) {
          await FileSystem.moveAsync({
            from: uri,
            to: newUri,
          });

          const asset = await MediaLibrary.createAssetAsync(newUri);
          await MediaLibrary.createAlbumAsync("AttendanceApp", asset, false);

          setSuccess(true);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleGetYearLevel = async () => {
    const { data, error } = await supabase.from("year_level").select("*");
    setYearLevels(data);
  }

  const handleGetData = async (grd_level) => {
    setLoading(true);
    setSuccess(false);
    let { data, error } = await supabase.from("enrolles").select("*").eq("year_level", grd_level);

    if (error) {
      console.error("Error on getting a data", error);
      return;
    }
    setLoading(false);
    setStudents(data);
    setSelectedLevel(grd_level);
  }

  const handleRenderData = ({item}) => {
    return (
      <TouchableOpacity
        key={item.id}
        onPress={() =>
          nav.navigate("ViewStudent", {
            id: item.id,
          })
        }
      >
        <View className="py-3 border-gray-100 border-b px-4 flex flex-row items-center justify-between">
          <View className="flex flex-row gap-x-2">
            <Text className="font-bold text-gray-700">{item.fullname}</Text>
            {item.current_status !== "ACTIVE" ? (
              <Text className="p-1 text-[8px] bg-red-400 text-white uppercase">Deactivated</Text>
            ) : (
              ""
            )}
          </View>
          <Ionicons name="arrow-forward" size={20} color="blue" />
        </View>
      </TouchableOpacity>
    );
  }

  const NavLink = ({ id, title, grd_level, isActive }) => {

    return (
      <TouchableOpacity
        key={id}
        onPress={async () => {
          await handleGetData(grd_level);
          setActive(id);
        }}
      >
        <View style={[styles.btnGrade, isActive ? styles.btnGradeActive : ""]}>
          <Text style={isActive ? { color: "#0068B8", fontWeight: "700" } : ""} className="text-[13px]">{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  useEffect(() => {
    const init = async () => {
      await handleGetData("GRD_7");
      await handleGetYearLevel();
    }

    init()
  },[])

  return (
    <View className="mx-5 mt-5">
      {success && (
        <Alert
          title="Success"
          message="Downdload complete, you can check your file at /Picture/AttendanceApp/"
          type="success"
        />
      )}
      {successUpload && (
        <Alert
          title="Success"
          message="Upload complete, you can check records by tapping grade levels."
          type="success"
        />
      )}
      <View className="flex flex-row justify-end items-center">
        <View className="flex flex-row gap-2">
          <TouchableOpacity
            className="h-[30px] w-auto px-3 flex items-center justify-center rounded-full bg-blue-800"
            onPress={handleBulkUpload}
          >
            <Text className="text-white text-[13px]">Bulk Upload</Text>
          </TouchableOpacity>
          <TouchableOpacity
            className="h-[30px] w-[30px] flex items-center justify-center rounded-full bg-blue-600"
            onPress={() => nav.navigate("AddStudent")}
          >
            <Ionicons name="add" size={13} color="white" />
          </TouchableOpacity>
        </View>
      </View>
      <View className="mt-3">
        <ScrollView horizontal={true}>
          <View className="flex flex-row pb-2">
            {yearLevels.map((item) => (
              <NavLink
                key={item.id}
                title={item.level_name}
                id={item.id}
                grd_level={item.level_code}
                isActive={active === item.id}
              />
            ))}
          </View>
        </ScrollView>
      </View>

      {loading ? (
        <ActivityIndicator style={{ marginTop: 40 }} />
      ) : (
        <View className="rounded-2xl bg-white mt-3 h-[500px]">
          <View className="flex flex-row items-center justify-between p-4 bg-[#F1F9FF] rounded-2xl rounded-b-none">
            <Text>{students.length} Students</Text>
            <TouchableOpacity
              onPress={handleBulkDownloadQRs}
              className="h-[30px] w-auto px-3 flex items-center justify-center rounded-full bg-[#1E8CE1]"
            >
              <Text className="text-white text-[13px]">Download QR</Text>
            </TouchableOpacity>
          </View>
          <FlatList data={students} renderItem={handleRenderData} />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  btnGrade: {
    borderRadius: 50,
    backgroundColor: "#fafafa",
    paddingHorizontal: 20,
    paddingVertical: 6,
    marginRight: 10,
    fontSize: 12
  },
  btnGradeActive: {
    backgroundColor: "#D2ECFF",
  },
});
export default Students