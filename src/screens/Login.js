import { useState } from "react";
import { View, Text, TextInput, TouchableOpacity, Alert, ScrollView } from "react-native";
import { supabase } from "../lib/supabase";

const Login = ({navigation}) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const handleLogin = async () => {
        console.log(username, password)
        setError("");
        let { data, error } = await supabase
            .from("users")
            .select("*")
            .eq("username", username)
            .eq("password", password);
        
        if (error) {
            console.log("Error", error);
            return;
        }

        if (data.length <= 0) {
            setError("Password and username mismatch. Please try again.");
            return;
        }

        navigation.navigate("Dashboard");
    }
  return (
    <ScrollView className="">
      <View className="bg-white h-[240px] flex justify-end items-center">
        <Text className="mb-5 text-[36px] font-semibold">Admin</Text>
      </View>
      <View className="flex gap-y-6 flex-col px-8 bg-gray-50 pt-10">
        <View className="flex gap-2 flex-col">
          <Text>Username</Text>
          <TextInput
            placeholder="enter username"
            onChange={(e) => setUsername(e.nativeEvent.text)}
            className="p-4 border border-slate-400 bg-white"
          ></TextInput>
        </View>
        <View className="flex gap-2 flex-col">
          <Text>Password</Text>
          <TextInput
            placeholder="enter username"
            onChange={(e) => setPassword(e.nativeEvent.text)}
            className="p-4 border border-slate-400 bg-white"
            secureTextEntry={true}
          ></TextInput>
        </View>
        <View>
          <TouchableOpacity
            className="p-5 bg-[#3290D3] rounded-lg"
            onPress={handleLogin}
          >
            <Text className="text-center text-white font-bold text-base">
              Login
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          {error && (
            <View className="p-3 bg-red-100 rounded-md">
              <Text className="text-red-500">{error}</Text>
            </View>
          )}
        </View>
      </View>
    </ScrollView>
  );
}

export default Login