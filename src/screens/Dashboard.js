import { View, Text, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";


const Dashboard = () => {

  const nav = useNavigation();

  return (
    <View className="mt-5 mx-5">
      <View className="grid gap-y-5">
        <TouchableOpacity
          className="px-5 py-3 bg-[#fff] rounded-md"
          onPress={() => nav.navigate("Students")}
        >
          <Text className="text-[16px] text-[#2BA495] mb-1 font-bold">
            Students
          </Text>
          <Text className="text-[12px] text-[#8E8E8E]">
            Add year level, subjects and student profile
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          className="px-5 py-3 bg-[#fff] rounded-md"
          onPress={() => nav.navigate("Teachers")}
        >
          <Text className="text-[16px] text-[#2BA495] mb-1 font-bold">
            Teachers
          </Text>
          <Text className="text-[12px] text-[#8E8E8E]">
            Register teacher with username and password.
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          className="px-5 py-3 bg-[#fff] rounded-md"
          onPress={() => nav.navigate("Scheduler")}
        >
          <Text className="text-[16px] text-[#2BA495] mb-1 font-bold">
            Scheduler
          </Text>
          <Text className="text-[12px] text-[#8E8E8E]">
            Browse teacher and schedule their classes.
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          className="px-5 py-3 bg-[#fff] rounded-md"
          onPress={() => nav.navigate("SubjectsPerLevel")}
        >
          <Text className="text-[16px] text-[#2BA495] mb-1 font-bold">
            Subjects per Year Level
          </Text>
          <Text className="text-[12px] text-[#8E8E8E]">
            Set the subjects per year level.
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          className="px-5 py-3 bg-[#fff] rounded-md"
          onPress={() => nav.navigate("Settings")}
        >
          <Text className="text-[16px] text-[#2BA495] mb-1 font-bold">
            Others
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Dashboard