import { View, Text, TouchableOpacity, TextInput, ScrollView,  } from "react-native";
import { useEffect, useState } from "react";
import DropDownPicker from "react-native-dropdown-picker";
import { supabase } from "../lib/supabase";
import { Alert } from "../components/Alert";

const AddStudent = () => {
  const [successSave, setSuccessSave] = useState(false);
  const [year, setYear] = useState([]);
  const [yearOpen, setYearOpen] = useState(false);
  const [yearVal, setYearVal] = useState("");
  const [section, setSection] = useState([
    { label: "Section A", value: "Section A" },
    { label: "Section B", value: "Section B" },
    { label: "Section C", value: "Section C" },
  ]);
  const [sectionOpen, setSectionOpen] = useState(false);
  const [sectionVal, setSectionVal] = useState("");

  // students state
  const [studentId, setStudentId] = useState("");
  const [fullname, setFullname] = useState("");
  const [parentName, setParentName] = useState("");
  const [parentNumber, setParentNumber] = useState("");

  const yearArray = [];

  const handleGetData = async () => {
    const { data, error } = await supabase
      .from("year_level")
      .select("*");
    
    if (error) {
      console.error(error);
      return;
    }

    data.map(item => {
      yearArray.push({
        label: item.level_name,
        value: item.level_code,
      });
    })

    setYear(yearArray);
  }

  const handleSave = async () => {
    const dataToInsert = [
      {
        fullname: fullname,
        guardian_name: parentName,
        guardian_contact: parentNumber,
        year_level: yearVal,
        qrcode: "QR:" + studentId + fullname,
        current_status: "ACTIVE",
        student_id: studentId,
        section_name: sectionVal,
      },
    ];

    // save to database
    const { error } = await supabase.from("enrolles").insert(dataToInsert);

    if (error) {
      console.error("Error on save enrolles", error);
      return;
    }

    console.log("Success on inserting enrolles.");
    setSuccessSave(true);
  }
  useEffect(() => {
    const init = async () => {
      await handleGetData();
    }
    init();
  }, []);

  return (
    <View>
      <View className="mx-5 mt-5">
        {successSave && (
          <Alert
            title="Success"
            message="Successfully added student."
            type="success"
          />
        )}
        <View className="grid gap-y-4">
          <TextInput
            onChange={(e) => setStudentId(e.nativeEvent.text)}
            placeholder="Student ID"
            className="px-4 py-3 border border-slate-600 rounded-md text-black bg-white"
          ></TextInput>
          <TextInput
            onChange={(e) => setFullname(e.nativeEvent.text)}
            placeholder="Full Name"
            className="px-4 py-3 border border-slate-600 rounded-md text-black bg-white"
          ></TextInput>
          <TextInput
            onChange={(e) => setParentName(e.nativeEvent.text)}
            placeholder="Parent Name"
            className="px-4 py-3 border border-slate-600 rounded-md text-black bg-white"
          ></TextInput>
          <TextInput
            onChange={(e) => setParentNumber(e.nativeEvent.text)}
            placeholder="Parent Number"
            className="px-4 py-3 border border-slate-600 rounded-md text-black bg-white"
          ></TextInput>
          <DropDownPicker
            open={yearOpen}
            setOpen={setYearOpen}
            items={year}
            setItems={setYear}
            placeholder="Select year level"
            value={yearVal}
            setValue={setYearVal}
          />
          <View className="relative z-30">
            <DropDownPicker
              open={sectionOpen}
              setOpen={setSectionOpen}
              items={section}
              setItems={setSection}
              placeholder="Select section"
              value={sectionVal}
              setValue={setSectionVal}
            />
          </View>
          <View>
            <TouchableOpacity
              className="p-5 bg-[#3290D3] rounded-lg"
              onPress={handleSave}
            >
              <Text className="text-center text-white font-bold text-base">
                Add Student Information
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default AddStudent;
