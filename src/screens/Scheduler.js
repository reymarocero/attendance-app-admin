import { View, Text, TextInput, TouchableOpacity, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { supabase } from "../lib/supabase";
import { useEffect, useState } from "react";

const Scheduler = () => {
  const nav = useNavigation();

  const [schedulesData, setSchedulesData] = useState([]);
  
  const getSchedulerData = async () => {
    const { data, error } = await supabase
      .from("teacher")
      .select(`
      id,
        full_name,
        scheduler(id)
      `);
    
    if (error) {
      console.error("Error on retrieving schedule data", error);
      return;
    }

    setSchedulesData(data);
  }

  const handleViewSchedule = (id) => {
    nav.navigate("ViewSchedule", {
      teacher_id: id
    });
  }

  const renderSchedules = ({ item }) => {
    return (
      <TouchableOpacity key={item.id} onPress={()=> handleViewSchedule(item.id)}>
        <View className="flex flex-row py-3 border-gray-100 border-b px-4 justify-between">
          <Text className="font-bold text-gray-700">{item.full_name}</Text>
          <Text className="font-bold text-gray-700">{item.scheduler.length} sched</Text>
        </View>
      </TouchableOpacity>
    );
  }

  useEffect(() => {
    const init = async () => {
      await getSchedulerData();
    }

    init();
  },[])
    return (
      <View className="mx-5 mt-5">
        <View className="rounded-2xl bg-white mt-5">
          <View className="flex flex-row items-center justify-between p-4 bg-[#F1F9FF] rounded-2xl rounded-b-none">
            <Text className="font-bold">Select Teacher</Text>
          </View>

          <FlatList
            data={schedulesData}
            renderItem={renderSchedules}
            className="mt-4"
          />
        </View>
      </View>
    );
}

export default Scheduler;