import { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, ScrollView, FlatList } from "react-native";
import { supabase } from "../lib/supabase";
import { useNavigation } from "@react-navigation/native";
import Ionicons from "@expo/vector-icons/Ionicons";
import dateFormat from "dateformat";

import { days } from "../constants/days";
const ViewSchedule = ({ route }) => {
    const nav = useNavigation();
    const { teacher_id } = route.params;
    const [schedulerInfo, setSchedulerInfo] = useState([]);
    const getDay = new Date().getDay();
    const [isId, setIsId] = useState(getDay);

    const getSchedules = async (day) => {
        if (!day) {
            day = new Date().getDay();
        }
        const { data, error } = await supabase
            .from("scheduler")
            .select(`section_name, start_time, end_time,what_day, teacher_id, subjects(subject_name), year_level(level_name)`)
            .eq("teacher_id", teacher_id)
            .eq("what_day", day)
        
        if (error) {
            console.error(error);
            return;
        }

        setSchedulerInfo(data);
            
    }

    useEffect(() => {
        const init = async () => {
            await getSchedules();
        }
        init();
    }, []);

    const RenderDays = ({whatDay, isActive, dayLabel}) => {
      return (
        <TouchableOpacity
          className={`p-2 rounded-full mr-2 ${
            isActive ? "bg-gray-400" : "bg-gray-100"
          }`}
          onPress={async () => {
            await getSchedules(whatDay);
            setIsId(whatDay);
          }}
        >
          <Text
            className={`${
              isActive ? "text-white" : "text-black"
            }`}
          >
            {dayLabel}
          </Text>
        </TouchableOpacity>
      );
    };

    const handleRenderView = ({ item }) => {
      return (
        <View key={item.id} className="flex flex-row py-3 border-gray-100 border-b px-4 justify-between w-full items-center">
          <View className="grid gap-y-1">
            <Text className="font-bold text-gray-700 text-[17px]">
              {item.year_level.level_name}
            </Text>
            <Text className="text-gray-500 text-[13px]">
              {item.subjects.subject_name}{" - "}
              {item.section_name}
            </Text>
          </View>
          <View>
            <Text className="font-bold text-gray-700">
              {dateFormat(item.start_time, "h:MM TT")} -{" "}
              {dateFormat(item.end_time, "h:MM TT")}
            </Text>
          </View>
        </View>
      );
    };

    return (
      <View className="mx-5 mt-5">
        <View className="rounded-2xl bg-white mt-5 w-full">
          <View className="p-4 bg-[#F1F9FF] rounded-2xl rounded-b-none flex flex-row justify-between items-center">
            <Text className="font-bold">Schedules</Text>
            <TouchableOpacity
              className="h-[40px] w-[40px] flex items-center justify-center rounded-full bg-cyan-700"
                onPress={() => nav.navigate("AddSchedule", {
                  teacher_id: teacher_id
              })}
            >
              <Ionicons name="add" size={20} color="white" />
            </TouchableOpacity>
          </View>
          <ScrollView horizontal={true} className="my-3">
            <View className="flex flex-row mx-4 pb-3">
              <TouchableOpacity
                onPress={async () => {
                  await getSchedules();
                  setIsId(getDay);
                }}
                className={`p-2 rounded-full mr-2 ${
                  isId === getDay ? "bg-gray-400" : "bg-gray-100"
                }`}
              >
                <Text
                  className={`${isId === getDay ? "text-white" : "text-black"}`}
                >
                  Today
                </Text>
              </TouchableOpacity>
              {days.map((item) => (
                <RenderDays
                  key={item.key}
                  whatDay={item.key}
                  dayLabel={item.value}
                  isActive={isId === item.key}
                />
              ))}
            </View>
          </ScrollView>
          {schedulerInfo.length > 0 ? (
            <FlatList
              data={schedulerInfo}
              renderItem={handleRenderView}
              className="w-full"
            />
          ) : (<Text className="text-center my-4">No schedules</Text>)}
        </View>
      </View>
    );
}

export default ViewSchedule;