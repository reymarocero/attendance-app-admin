import { useState } from "react";
import { View, Text, TouchableOpacity, TextInput } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import { Alert } from "../components/Alert";
import { supabase } from "../lib/supabase";

const SubjectsPerLevel = () => {
  const [subjectCode, setSubjectCode] = useState([]);
  const [yearLevelCode, setYearLevelCode] = useState([]);

  const [subjectCodeVal, setSubjectCodeVal] = useState("");
  const [yearLevelCodeVal, setYearLevelCodeVal] = useState("");

  const [openSubject, setOpenSubject] = useState(false);
  const [openYearLevelCode, setOpenYearLevelCode] = useState(false);

  const [isExist, setIsExist] = useState(false);
  const [success, setSucess] = useState(false);

  const [addSubjectCode, setAddSubjectCode] = useState("");
  const [addSubjectName, setAddSubjectName] = useState("");
  const [showAdd, setShowAdd] = useState(false);

  // get subjects
  const handleSubjects = async () => {
    const { data, error } = await supabase.from("subjects").select("*");

    if (error) {
      console.error(error);
      return;
    }
    const temp = [];
    data.map((item) => {
      temp.push({
        label: item.subject_name,
        value: item.subject_code,
      });
    });

    setSubjectCode(temp);
  };

  // get year level
  const handleYearLevel = async () => {
    const { data, error } = await supabase.from("year_level").select("*");

    if (error) {
      console.error(error);
      return;
    }
    const temp = [];
    data.map((item) => {
      temp.push({
        label: item.level_name,
        value: item.level_code,
      });
    });

    setYearLevelCode(temp);
  };

  const checkIfExist = async () => {
    const { data, error } = await supabase
      .from("subjects_per_level")
      .select("*")
      .eq("subjects", subjectCodeVal)
      .eq("level_code", yearLevelCodeVal);

    if (data.length > 0) {
      setIsExist(true);
      return;
    }

    setIsExist(false);
  };

  const handleInsert = async () => {
    const { error } = await supabase.from("subjects_per_level").insert({
      subjects: subjectCodeVal,
      level_code: yearLevelCodeVal,
    });

    if (error) {
      console.error(error);
      return;
    }
    console.log("Success");
    setYearLevelCodeVal("");
    setSubjectCodeVal("");
    setSucess(true);
  };

  const handleAddSubject = async () => {
    const { error } = await supabase
      .from("subjects")
      .insert({
        subject_code: addSubjectCode,
        subject_name: addSubjectName
      });
    
    if (error) {
      console.error(error);
      return;
    }

    setSucess(true);
    setShowAdd(false);
    await handleSubjects();
  }

  useState(() => {
    const init = async () => {
      await handleSubjects();
      await handleYearLevel();
    };

    init();
  }, []);

  return (
    <View className="mt-5 mx-5">
      {success && (
        <Alert title="Success" message="Successfully added." type="success" />
      )}
      {isExist && (
        <Alert
          title="Error"
          message="Subject and Year Level is already exist. Please select different value."
          type="error"
        />
      )}
      {!showAdd && (
        <View>
          <View className="flex flex-row justify-between mb-3">
            <Text>Select Subject</Text>
            <TouchableOpacity onPress={() => setShowAdd(true)}>
              <Text className="text-cyan-600 font-bold underline">
                Add new Subject
              </Text>
            </TouchableOpacity>
          </View>
          <DropDownPicker
            open={openSubject}
            setOpen={setOpenSubject}
            items={subjectCode}
            setItems={setSubjectCode}
            value={subjectCodeVal}
            setValue={setSubjectCodeVal}
            placeholder="Select subject"
            onChangeValue={async () => checkIfExist()}
            style={{
              borderWidth: 0,
              marginBottom: 20,
              position: "relative",
              zIndex: 30,
            }}
            dropDownContainerStyle={{ borderColor: "white" }}
          />
          <Text className="mb-3 relative z-10">Select Year Level</Text>
          <DropDownPicker
            open={openYearLevelCode}
            setOpen={setOpenYearLevelCode}
            items={yearLevelCode}
            setItems={setYearLevelCode}
            value={yearLevelCodeVal}
            setValue={setYearLevelCodeVal}
            placeholder="Select level"
            onChangeValue={async () => checkIfExist()}
            style={{
              borderWidth: 0,
              marginBottom: 20,
              position: "relative",
              zIndex: 20,
            }}
            dropDownContainerStyle={{ borderColor: "white" }}
          />
          <TouchableOpacity
            className="py-5 rounded-lg bg-emerald-700"
            disabled={isExist ? true : false}
            style={isExist ? { opacity: 0.5 } : { opacity: 1 }}
            onPress={handleInsert}
          >
            <Text className="text-white text-center font-bold">Add</Text>
          </TouchableOpacity>
        </View>
      )}

      {showAdd && (
        <View>
          <View className="bg-white rounded-md z-40 w-full p-5">
            <Text className="mb-3">Subject Code</Text>
            <TextInput
              onChange={(e) => setAddSubjectCode(e.nativeEvent.text)}
              placeholder="Subject code"
              className="px-4 py-2 border border-slate-600 rounded-md text-black bg-white mb-3"
            ></TextInput>
            <Text className="mb-2">Subject Name</Text>
            <TextInput
              onChange={(e) => setAddSubjectName(e.nativeEvent.text)}
              placeholder="Subject name"
              className="px-4 py-2 border border-slate-600 rounded-md text-black bg-white mb-3"
            ></TextInput>
            <View className="flex flex-row justify-between">
              <TouchableOpacity
                className="py-2 mt-3 rounded-lg bg-cyan-700 w-auto px-5"
                onPress={handleAddSubject}
              >
                <Text className="text-white text-center font-bold">
                  Add Subject
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                className="py-2 mt-3 px-5 rounded-lg bg-red-400"
                onPress={() => setShowAdd(false)}
              >
                <Text className="text-white text-center font-bold">Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

export default SubjectsPerLevel;
