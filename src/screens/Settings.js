import { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Alert } from "../components/Alert";
import { supabase } from "../lib/supabase";

const Settings = () => {
    const [success, setSuccess] = useState(false);

    const handleTest = async () => {
        const {data, error } = await supabase
            .from("sms_schedule")
            .update({
                test: 1
            })
            .eq("id", 3);
        
        if (error) {
            console.error(error);
        }
        console.log(data);
        setSuccess(true);
    }
    return (
      <View className="mt-5 mx-5">
        {success && (
          <Alert title="Success" message="Testing..." type="warning" />
        )}
        <TouchableOpacity
          className="px-4 py-2 bg-red-400"
                onPress={async () => { await handleTest(); }}
        >
          <Text className="text-white">Test sms</Text>
        </TouchableOpacity>
      </View>
    );
}

export default Settings;