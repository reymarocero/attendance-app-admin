export const qrPdfHTML = (data) => {

    const header = `
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
        </head>
        <body style="min-width: 800px; margin: 40px;">
            <div style="display: flex; gap: 20px; flex-wrap: wrap;">`;

    const footer = `</div></body></html>`;

    let ret = ``;
    
    ret += header;

    data.map(item => {
        ret += `
            <div style="border: 1px solid gray; border-radius: 10px; width: 200px;">
                <div style="display: flex; justify-content: center; text-align: center;padding: 10px;">
                    <img src="https://via.placeholder.com/150" alt="" style="border-radius: 50px; border: 1px solid; width: 100px">
                </div>
                <div style="display: flex; justify-content: center; margin: 20px 0px;">
                    <img src="http://api.qrserver.com/v1/create-qr-code/?data=${item.qrcode}&size=150x150" alt="">
                </div>
                <span style="display: block; text-align: center; font-size: 18px; font-weight: bold;">
                    ${item.fullname}
                </span>
                <span style="display: block; text-align: center; font-size: 15px; margin-bottom: 30px; font-weight: 500; color: #818181">
                    ${item.year_level} - ${item.section_name}
                </span>
            </div>
        `;
    })

    ret += footer;

    return ret;
}