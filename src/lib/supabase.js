import "react-native-url-polyfill/auto";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { createClient } from "@supabase/supabase-js";

console.log(process.env.REACT_NATIVE_SUPABASE_URL);

export const supabase = createClient(
  process.env.REACT_NATIVE_SUPABASE_URL,
  process.env.REACT_NATIVE_SUPABASE_KEY,
  {
    auth: {
      storage: AsyncStorage,
    },
  }
);
