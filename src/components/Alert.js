import { View, Text } from "react-native";

export const Alert = (props) => {
    let classVariable ="bg-cyan-100 border-l-4 border-cyan-500 text-cyan-700 p-4 mb-4";

    if (props.type === "success") {
      classVariable =
        "bg-green-100 border-l-4 border-green-500 text-green-700 p-4 mb-4";
    } else if (props.type === "warning") {
      classVariable =
        "bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4 mb-4";
    } else if (props.type === "error") {
      classVariable =
        "bg-red-100 border-l-4 border-red-500 text-red-700 p-4 mb-4";
    }
    return (
      <View className={classVariable}>
            <Text className="font-bold mb-2">{ props.title}</Text>
            <Text>{ props.message}</Text>
      </View>
    );
}