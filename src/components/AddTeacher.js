import { useNavigation } from "@react-navigation/native";
import { useState } from "react";
import { View, Text, TouchableOpacity, TextInput } from "react-native";
import { supabase } from "../lib/supabase";

  const AddTeacher = () => {
    const nav = useNavigation();

    const [fullname, setFullname] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleAddTeacher = async () => {
      const { error } = await supabase
          .from("teacher")
          .insert({ full_name: fullname, username: username, password: password });
      
      if (error) {
        console.error("Error on inserting data", error);
        return;
      }

      nav.navigate("Teachers");
    }

    return (
      <View className="mx-5 mt-5">
        <View className="grid gap-4">
          <View>
            <Text className="mb-2">Full Name</Text>
            <TextInput
              placeholder="Enter Full Name"
              className="p-4 border border-slate-600 rounded-md text-black"
              onChange={(e) => setFullname(e.nativeEvent.text)}
            ></TextInput>
          </View>
          <View>
            <Text className="mb-2">Username</Text>
            <TextInput
              placeholder="Username"
              className="p-4 border border-slate-600 rounded-md text-black"
              onChange={(e) => setUsername(e.nativeEvent.text)}
            ></TextInput>
          </View>
          <View>
            <Text className="mb-2">Password</Text>
            <TextInput
              placeholder="Password"
              className="p-4 border border-slate-600 rounded-md text-black"
              onChange={(e) => setPassword(e.nativeEvent.text)}
            ></TextInput>
          </View>
          <TouchableOpacity
            className="py-5 rounded-lg bg-emerald-700"
            onPress={handleAddTeacher}
          >
            <Text className="text-white text-center font-bold">
              Add Teacher
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
}

export default AddTeacher;