import { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, FlatList, TextInput } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { supabase } from '../lib/supabase';
import DateTimePicker from "@react-native-community/datetimepicker";
import dateFormat from 'dateformat';
import { Alert } from './Alert';


const AddSchedule = ({route}) => {

  const [levelCode, setLevelCode] = useState("");
  const [subject, setSubject] = useState("");
  const [day, setDay] = useState("");
  const [timeStart, setTimeStart] = useState(new Date("1/1/1980 07:00:00 AM"));
  const [timeEnd, setTimeEnd] = useState(new Date("1/1/1980 08:00:00 AM"));
  const [dataTeacherState, setDataTeacherState] = useState([]);
  const [dataYearLevelState, setDataYearLevelState] = useState([]);
  const [dataSubjectState, setDataSubjectState] = useState([]);
  const [currentSchedule, setCurrentSchedule] = useState([]);
  const [dataDay, setDataDay] = useState([
    { value: 1, label: "MONDAY" },
    { value: 2, label: "TUESDAY" },
    { value: 3, label: "WEDNESDAY" },
    { value: 4, label: "THURSDAY" },
    { value: 5, label: "FRIDAY" },
    { value: 6, label: "SATURDAY" },
    { value: 0, label: "SUNDAY" },
  ]);
  const [sectionValue, setSectionValue] = useState("");
  const [section, setSection] = useState([
    { value: "Section A", label: "Section A" },
    { value: "Section B", label: "Section B" },
    { value: "Section C", label: "Section C" }
  ]);
  const [openTeacher, setOpenTeacher] = useState(false);
  const [openYearLevel, setOpenYearLevel] = useState(false);
  const [openSubject, setOpenSubject] = useState(false);
  const [openDay, setOpenDay] = useState(false);
  const [openSection, setOpenSection] = useState(false);
  const [exist, setExist] = useState(false);
  const [success, setSuccess] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showDatePicker2, setShowDatePicker2] = useState(false);

  const dataYearLevel = [];
  const dataSubject = [];

  const { teacher_id } = route.params;
  
  const getYearLevelData = async () => {
    const { data, error } = await supabase.from("year_level").select("*");

    if (error) {
      console.error("Error on retrieving year level data", error);
      return;
    }

    data.map((item) => {
      dataYearLevel.push({
        value: item.level_code,
        label: item.level_name,
      });
    });

    setDataYearLevelState(dataYearLevel);

  };

  const getSubjectData = async () => {
    const { data, error } = await supabase
      .from("subjects_per_level")
      .select(`level_code, subjects(subject_code, subject_name)`)
      .eq("level_code", levelCode);

    if (error) {
      console.error("Error on retrieving year subject data", error);
      return;
    }

    data.map((item) => {
      dataSubject.push({
        value: item.subjects.subject_code,
        label: item.subjects.subject_name,
      });
    });

    setDataSubjectState(dataSubject);
  };

  const handleAddSchedule = async () => {
    // check if schedule is exist
    const { data, error_schedule } = await supabase
      .from("scheduler")
      .select("*")
      .eq("teacher_id", teacher_id)
      .eq("what_day", day)
      .eq("subject_code", subject)
      .eq("level_code", levelCode)
      .eq("section_name", sectionValue);
    
    if (error_schedule) {
      console.error(error_schedule);
      return;
    }
    
    if (data.length > 0) {
      setExist(true);
      setSuccess(false);
      return;
    }

    const { error } = await supabase.from("scheduler").insert({
      teacher_id: teacher_id,
      start_time: timeStart,
      end_time: timeEnd,
      what_day: day,
      subject_code: subject,
      level_code: levelCode,
      section_name: sectionValue
    });
    
    if (error) {
      console.error("Error on creating schedule", error);
      return;
    }

    setSuccess(true);
    setExist(false);
    console.log("Successfully adding schedule");
  }

  useEffect(() => {
    const init = async () => {
      await getYearLevelData();
    }

    init();
  },[])
  
    
    return (
      <View className="mx-5 mt-5">
        {exist && (
          <Alert
            title="Error"
            message="Schedule already exist. Please try again."
            type="error"
          />
        )}
        {success && (
          <Alert
            title="Success"
            message="Successfully added schedule."
            type="success"
          />
        )}
        <View className="grid">
          <Text className="mb-2">Year Level</Text>
          <DropDownPicker
            open={openYearLevel}
            value={levelCode}
            items={dataYearLevelState}
            setOpen={setOpenYearLevel}
            setValue={setLevelCode}
            setItems={setDataYearLevelState}
            onChangeValue={async () => {
              await getSubjectData();
            }}
            placeholder="Select Year Level"
            style={{
              borderWidth: 0,
              marginBottom: 20,
              position: "relative",
              zIndex: 20,
            }}
            dropDownContainerStyle={{ borderColor: "white" }}
          />

          <Text className="mb-2">Section</Text>
          <DropDownPicker
            open={openSection}
            value={sectionValue}
            items={section}
            setOpen={setOpenSection}
            setValue={setSectionValue}
            setItems={setSection}
            placeholder="Select Section"
            style={{
              borderWidth: 0,
              marginBottom: 20,
              position: "relative",
              zIndex: 20,
            }}
            dropDownContainerStyle={{ borderColor: "white" }}
          />
          <Text className="mb-2">Subject</Text>
          <DropDownPicker
            open={openSubject}
            value={subject}
            items={dataSubjectState}
            setOpen={setOpenSubject}
            setValue={setSubject}
            setItems={setDataSubjectState}
            placeholder="Select Subject"
            style={{
              borderWidth: 0,
              marginBottom: 20,
              position: "relative",
              zIndex: 20,
            }}
            dropDownContainerStyle={{ borderColor: "white" }}
          />
          <DropDownPicker
            open={openDay}
            value={day}
            items={dataDay}
            setOpen={setOpenDay}
            setValue={setDay}
            setItems={setDataDay}
            placeholder="Select Day"
            textStyle={{ borderWidth: 0, borderColor: "white" }}
            style={{
              borderWidth: 0,
              marginBottom: 20,
              position: "relative",
              zIndex: 20,
            }}
            dropDownContainerStyle={{ borderColor: "white" }}
          />
          <View className="flex flex-row gap-x-2 items-center relative z-0 mb-10">
            <View className="flex flex-row gap-2 items-center">
              <Text>From</Text>
              <Text className="bg-white p-3">
                {dateFormat(timeStart, "shortTime")}
              </Text>
              <TouchableOpacity
                onPress={() => setShowDatePicker(true)}
                className="bg-cyan-600 p-3 rounded-sm"
              >
                <Text className="text-white">Set</Text>
              </TouchableOpacity>
              {Platform.OS === "ios" || showDatePicker ? (
                <DateTimePicker
                  mode="time"
                  value={timeStart}
                  onChange={(event, selectedTime) => {
                    setShowDatePicker(false);
                    setTimeStart(selectedTime);
                  }}
                />
              ) : (
                ""
              )}
            </View>
            <View className="flex flex-row gap-2 items-center">
              <Text>To</Text>
              <Text className="bg-white p-3">
                {dateFormat(timeEnd, "shortTime")}
              </Text>
              <TouchableOpacity
                onPress={() => setShowDatePicker2(true)}
                className="bg-cyan-600 p-3 rounded-sm"
              >
                <Text className="text-white">Set</Text>
              </TouchableOpacity>
              {Platform.OS === "ios" || showDatePicker2 ? (
                <DateTimePicker
                  mode="time"
                  value={timeEnd}
                  onChange={(event, selectedTime) => {
                    setShowDatePicker2(false);
                    setTimeEnd(selectedTime);
                  }}
                />
              ) : (
                ""
              )}
            </View>
          </View>
          <View className="relative z-auto">
            <TouchableOpacity
              className="py-5 rounded-lg bg-emerald-700"
              onPress={handleAddSchedule}
            >
              <Text className="text-white text-center font-bold">
                Add Schedule
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  boxStyle: {
    borderRadius: 4,
    borderColor: "#475569",
    borderWidth: 2,
  },
  dropDownStyle: {
    borderRadius: 4,
    borderWidth: 2,
    borderColor: "#475569",
  },
});

export default AddSchedule