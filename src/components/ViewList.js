import { FlatList, View, Text, TouchableOpacity, ScrollView } from "react-native";
import dateFormat from "dateformat";
import { useNavigation } from "@react-navigation/native";
import Ionicons from "@expo/vector-icons/Ionicons";
import { days } from "../constants/days";
import { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

function ViewList (props) {
  const nav = useNavigation();
  const { data, title } = props;

  const handleRenderView = ({ item }) => {
    return (
      <View className="flex flex-row py-3 border-gray-100 border-b px-4 justify-between w-full">
        <View className="grid gap-y-1">
          <Text className="font-bold text-gray-700 text-[17px]">
            {item.year_level.level_name}
          </Text>
          <Text className="text-gray-500 text-[13px]">
            {item.subjects.subject_name}
            {item.what_day}
          </Text>
        </View>
        <View>
          <Text className="font-bold text-gray-700">
            {dateFormat(item.start_time, "h:MM TT")} -{" "}
            {dateFormat(item.end_time, "h:MM TT")}
          </Text>
        </View>
      </View>
    );
  };

    return (
      <View className="mx-5 mt-5">
        <View className="rounded-2xl bg-white mt-5 w-full">
          <View className="p-4 bg-[#F1F9FF] rounded-2xl rounded-b-none flex flex-row justify-between items-center">
            <Text className="font-bold">{title}</Text>
            <TouchableOpacity
              className="h-[40px] w-[40px] flex items-center justify-center rounded-full bg-cyan-700"
              onPress={() => nav.navigate("AddSchedule")}
            >
              <Ionicons name="add" size={20} color="white" />
            </TouchableOpacity>
          </View>
          <ScrollView horizontal={true} className="my-3">
            <View className="flex flex-row mx-4 pb-3">
              <TouchableOpacity
                className="p-2 rounded-full bg-gray-400 mr-2"
              >
                <Text className="text-white">Today</Text>
              </TouchableOpacity>
              <RenderDays />
            </View>
          </ScrollView>

          {data.map(item => (
            <Text key={item.id}>{item.what_day }</Text>
          ))
          }

          <FlatList
            data={data}
            renderItem={handleRenderView}
            className="w-full"
          />
        </View>
      </View>
    );
}

export default ViewList;